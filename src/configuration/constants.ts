/** Rutas de navegacion de la App */
enum Pages {
  LOGINPAGE = "/Login",
  HOMEPAGE = "/",
  EMPLOYEEPAGE = "/Employee",
}

export { Pages };
