import { Pages } from "configuration/constants";
import React from "react";
import { useNavigate, useLocation, Navigate, Outlet } from "react-router-dom";

const PrivateRouter = () => {
  const navigate = useNavigate();
  const location = useLocation();
  return <Navigate to={Pages.LOGINPAGE} replace />;
  //return accessToken ? <Outlet /> : <Navigate to={Pages.LOGINPAGE} replace />;
};

export default PrivateRouter;
