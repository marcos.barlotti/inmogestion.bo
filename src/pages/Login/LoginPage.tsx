import { authenticationMessages } from "configuration/messages";
import { CatastroDTO } from "interfaces/backendApi";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { catastroService } from "services/BackendApi";

const LoginPage = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [list, setList] = useState<CatastroDTO[]>();
  const from = ((location.state as any)?.from?.pathname as string) || "/";

  const getList = async () => {
    catastroService.GetAll().then((response) => {
      setList(response);
    });
  };

  useEffect(() => {
    getList();
  }, []);

  return (
    <div>
      {list?.map((item, index) => (
        <label key={`${index}${item.nomenclatura}`}>{item.nomenclatura}</label>
      ))}
    </div>
  );
};

export default LoginPage;
