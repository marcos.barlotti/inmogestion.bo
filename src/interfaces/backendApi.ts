export interface CatastroDTO {
  id: number;
  seccion: number;
  manzana: string;
  parcela: string;
  nomenclatura: string;
}
