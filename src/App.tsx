import React, { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import { RootRouter } from "routers";
import { Pages } from "configuration/constants";

const App = (): JSX.Element => {
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL || Pages.HOMEPAGE}>
      <RootRouter />
    </BrowserRouter>
  );
};

export default App;
