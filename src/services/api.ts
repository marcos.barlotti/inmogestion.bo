import axios, { AxiosError, AxiosRequestConfig, Method } from "axios";
import { Header } from "interfaces/services";
import { getItem } from "utils/storage";

const config = {
  baseURL: process.env.REACT_APP_BACKEND_BASEURL,
};

export const axiosInstance = axios.create(config);

/**
 * llama al backend
 * @param {string} endpoint Endpoint path
 * @param {string} method HTTP Method
 * @param {Header?} header Headers si los necesitase
 * @param {unknown?} body Request body
 * @returns {T} respuesta del servicio
 */
export const BackendInvoke = async <T>(
  endpoint: string,
  method: string,
  header?: Header,
  body?: unknown,
): Promise<T> => {
  try {
    //const token = getItem("@accessToken");
    const formHeaders = setRequestHeaders(header, undefined);
    const requestConfig: AxiosRequestConfig = {
      method: method as Method,
      url: endpoint,
      data: body,
      headers: formHeaders,
    };
    console.log("Request URL", endpoint);
    const response = await axiosInstance.request<T>(requestConfig);
    return response?.data;
  } catch (err) {
    console.log("api call", err);
    const newError = err as AxiosError;
    throw newError;
  }
};

/**
 * Crea los cabezales para el request HTTP
 * @param {Header} headers Headers en caso de ser necesario
 * @param {string} token token de la app
 */
export const setRequestHeaders = (
  header?: Header,
  token?: string | null,
): Header => {
  const formHeaders = header || headers;

  if (token && !header) {
    return {
      ...formHeaders,
      Authorization: `Bearer ${token}`,
    };
  }

  return formHeaders;
};

export const headers: Header = {
  Accept: "application/json",
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*",
};
