import { CatastroDTO } from "interfaces/backendApi";
import { BackendInvoke } from "services/api";

const catastroControllerName = "Catastro";

/**
 * Obtiene el listado de catastros
 * @returns {CatastroDTO[]} listado
 */
const GetAll = (): Promise<CatastroDTO[]> =>
  BackendInvoke<CatastroDTO[]>(
    `${catastroControllerName}/GetAll`,
    "GET",
    undefined,
    undefined,
  );

export default { GetAll };
